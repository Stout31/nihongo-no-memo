import axios from "axios";
import { API, BEARER } from "../constant";
import { getToken } from "../helpers";

const token = getToken();

export const getVocabularies = async ({ type }) => {
  let filters = "";
  switch (type) {
    case "numbers":
      filters = "filters[type][$eq]=number&";
      break;
    case "dates":
      filters =
        "filters[$or][1][type][$eq]=minute&" +
        "filters[$or][2][type][$eq]=hour&" +
        "filters[$or][3][type][$eq]=day&" +
        "filters[$or][4][type][$eq]=week&" +
        "filters[$or][5][type][$eq]=month&";
      break;
    default:
      filters =
        "filters[$and][0][type][$ne]=number&" +
        "filters[$and][1][type][$ne]=minute&" +
        "filters[$and][2][type][$ne]=hour&" +
        "filters[$and][3][type][$ne]=day&" +
        "filters[$and][4][type][$ne]=week&" +
        "filters[$and][5][type][$ne]=month&";
      break;
  }

  let url = "/api/vocabularies?";
  let pageSize = "pagination[pageSize]=";
  let page = "&pagination[page]=";

  return await axios.get(`${API}${url}${filters}${pageSize}1${page}1`).then(
    ({
      data: {
        meta: {
          pagination: { total },
        },
      },
    }) =>
      Promise.all(
        [...Array((total - 1 - ((total - 1) % 100)) / 100 + 1).keys()].map(
          (i) =>
            axios.get(`${API}${url}${filters}${pageSize}100${page}${i + 1}`)
        )
      ).then((data) =>
        data.reduce((acc, curr) => [...acc, ...curr.data.data], [])
      )
  );
};

export const postVocabularies = async (vocabulary) => {
  return await axios
    .post(
      `${API}/api/vocabularies`,
      { data: vocabulary },
      { headers: { Authorization: `${BEARER} ${token}` } }
    )
    .then(({ data }) => data)
    .catch((err) => console.error(err))
    .finally(() => {});
};

export const putVocabularies = async (id, vocabulary) => {
  const result = await axios
    .put(
      `${API}/api/vocabularies/${id}`,
      { data: vocabulary },
      { headers: { Authorization: `${BEARER} ${token}` } }
    )
    .then(({ data }) => data)
    .catch((err) => {
      console.error(err);
    })
    .finally(() => {});
  return result;
};

export const deletePicture = (id) => {
  axios
    .delete(`${API}/api/pictures/${id}`, {
      headers: { Authorization: `${BEARER} ${token}` },
    })
    .then(({ data }) => data)
    .catch((err) => {
      console.error(err);
    })
    .finally(() => {});
};
