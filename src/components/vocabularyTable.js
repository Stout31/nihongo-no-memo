import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Furigana from "../components/furigana";
import Box from "@mui/material/Box";

const VocabularyTable = ({ types, data, learn, setLearn }) => {
  const updateLearn = (id) => {
    if (id)
      setLearn(
        learn.indexOf(id) > -1
          ? learn.reduce((a, c) => [...a, ...(c === id ? [] : [c])], [])
          : [...learn, id]
      );
  };

  const updateLearnByType = (type) => {
    const ids = data.filter((d) => d.attributes.type === type).map((d) => d.id);
    setLearn(
      ids.every((id) => learn.indexOf(id) > -1)
        ? learn.reduce(
            (a, c) => [...a, ...(ids.indexOf(c) > -1 ? [] : [c])],
            []
          )
        : [...new Set([...learn, ...ids])]
    );
  };

  const typeName = (type) => {
    switch (type) {
      case "number":
        return "nombres";
      case "minute":
        return "minutes";
      case "hour":
        return "heures";
      case "day":
        return "jours";
      case "week":
        return "semaine";
      case "month":
        return "mois";
      default:
        return type;
    }
  };

  return (
    <TableContainer sx={{ height: "100%" }}>
      {data ? (
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {["Japonais", "Romanji", "Français"].map((column, index) => (
                <TableCell
                  key={`column${index}`}
                  padding="none"
                  align="center"
                  sx={{ width: "33%" }}
                >
                  {column}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>

          {types.map((type, index) => (
            <TableBody key={`type${index}`}>
              <TableRow>
                <TableCell
                  padding="none"
                  align="center"
                  colSpan={3}
                  sx={{
                    cursor: "pointer",
                    backgroundColor: "#121212",
                    "&:hover": {
                      backgroundColor: "#12121290",
                    },
                    "&:first-letter": {
                      textTransform: "uppercase",
                    },
                  }}
                  onClick={() => updateLearnByType(type)}
                >
                  {typeName(type)}
                </TableCell>
              </TableRow>
              {data
                .filter((d) => d.attributes.type === type)
                .map(({ id, attributes }, index) => {
                  return (
                    <TableRow
                      key={`row${index}`}
                      sx={{
                        cursor: "pointer",
                        "&:hover": {
                          backgroundColor: "#ffffff20",
                        },
                        ...(learn.indexOf(id) > -1
                          ? {
                              backgroundColor: "#0d6efd50",
                              "&:hover": {
                                backgroundColor: "#0d6efd80",
                              },
                            }
                          : {}),
                      }}
                      onClick={() => updateLearn(id)}
                    >
                      <TableCell padding="none" align="center">
                        <Furigana center text={attributes.japanese} />
                      </TableCell>
                      <TableCell padding="none" align="center">
                        {attributes.romanji}
                      </TableCell>
                      <TableCell padding="none" align="center">
                        {attributes.french}
                      </TableCell>
                    </TableRow>
                  );
                })}
              <TableRow>
                <TableCell variant="footer" colSpan={6} />
              </TableRow>
            </TableBody>
          ))}
        </Table>
      ) : (
        <Box
          sx={{
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          Aucunes données
        </Box>
      )}
    </TableContainer>
  );
};

export default VocabularyTable;
