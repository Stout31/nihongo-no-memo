import styled from "styled-components";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const StyledMainPaper = styled(Paper)`
  scroll-margin: 70px;
  padding: 10px;
  margin-bottom: 10px;

  @media (min-width: 641px) {
    padding: 20px;
  }
`;

const StyledPaper = styled(Paper)`
  padding: 10px;

  @media (min-width: 641px) {
    padding: 20px;
    width: fit-content;
  }
`;

const StyledBox = styled(Box)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 10px;
`;

const Notes = () => {
  const data = [
    { jp: "うけとりました。", fr: "Je l'ai reçu." },
    { jp: "もうすぐですね！", fr: "Bientôt disponible !" },
    { jp: "ゆうがた", fr: "soirée" },
    { jp: "ミュンヘン", fr: "Munich" },
    { jp: "にづくりをします。", fr: "Faire les valises." },
    { jp: "どちらに　いかれますか？", fr: "Où allez-vous ?" },
    { jp: "どちらまでですか？", fr: "Où allez-vous ?" },
    {
      jp: "〇〇ホテルまで　おねがいします。",
      fr: "À l'hôtel 00, s'il vous plaît.",
    },
    { jp: "たまごやき", fr: "Œufs et omelettes" },
    { jp: "なんめいさまですか。", fr: "Vous êtes combien de personnes ?" },
    { jp: "ふたりです。", fr: "Deux." },
    { jp: "おたばこは　おすいになりますか？", fr: "Est-ce que vous fumez ?" },
    { jp: "きんえんせき", fr: "Espace non-fumeur." },
    { jp: "きつえんせき", fr: "Espace fumeur" },
    { jp: "いいえ、すいません。", fr: "Non, je ne fume pas." },
    {
      jp: "きんえんせき　おねがいします。",
      fr: "Espace non-fumeur s'il vous plait.",
    },
    { jp: "おみずをください。", fr: "De l'eau s'il vous plait." },
    {
      jp: "ごちゅうもんは　おきまりですか？",
      fr: "Est-ce que vous avez choisi ?",
    },
    {
      jp: "ほかに　なにか　ごちゅうもんは　ありますか？",
      fr: "Ce sera tout ?",
    },
    { jp: "いじょうです。", fr: "Oui, c'est tout." },
    { jp: "おかいけい", fr: "L'addition" },
    { jp: "にづくりをしました。", fr: "J'ai fait mes valises" },
    { jp: "こじんてきなしごと", fr: "Travail personnel" },
    { jp: "りんごジュース", fr: "jus de pomme" },
    { jp: "コーラ", fr: "cola" },
    { jp: "おみそしる", fr: "Soupe miso" },
    { jp: "けっこうです。", fr: "Non, merci." },
    { jp: "おにく　おさかな", fr: "Viande Poisson" },
    {
      jp: "わしょく　ようしょく",
      fr: "Nourriture japonaise Nourriture occidentale",
    },
    { jp: "つめたい　おちゃ", fr: "Thé glacé" },
    { jp: "あたたかい　おちゃ", fr: "Thé chaud" },
    { jp: "コーヒー", fr: "café" },
    { jp: "こうちゃ", fr: "Thé noir" },
    { jp: "おちゃ🍵", fr: "Thé vert🍵" },
    { jp: "ミルク　さとう", fr: "Lait, sucre." },
    { jp: "はい　おねがいします。", fr: "Oui, s'il vous plaît." },
    { jp: "はい、どうぞ", fr: "Oui, s'il vous plaît." },
    {
      jp: "にほんに　くるのは　はじめて　ですか？",
      fr: "C'est la première fois que vous venez au Japon ?",
    },
    {
      jp: "(ご)りょこうの　もくてきは　なんですか？",
      fr: "Quel est le but de votre voyage ?",
    },
    { jp: "かんこう　です。", fr: "Le tourisme." },
    {
      jp: "どのくらい　たいざいする　よてい　ですか？",
      fr: "Combien de temps resterez-vous ?",
    },
    { jp: "2しゅうかん", fr: "2 semaines" },
    {
      jp: "かえりの　こうくうけんは　もっていますか？",
      fr: "Avez-vous un document de voyage de retour ?",
    },
    { jp: "しんこくする", fr: "Faire une déclaration." },
    { jp: "しんこくします", fr: "Je vais faire une déclaration" },
    { jp: "ぜいきん", fr: "La taxe" },
    {
      jp: "スーツケースをあけてください。",
      fr: "Veuillez ouvrir votre valise.",
    },
    {
      jp: "なにか　しんこくするものは　ありますか？",
      fr: "Avez-vous quelque chose à déclarer ?",
    },
    { jp: "いいえ　ありません。", fr: "Non, je n'ai rien." },
    { jp: "おさけを　もっていますか？", fr: "Avez-vous de l'alcool ?" },
    { jp: "なまえ", fr: "Prénom" },
    { jp: "みょうじ", fr: "Nom" },
    {
      jp: "おきゃくさま　の　おへや　は　〇〇ごうしつ　です。",
      fr: "Votre chambre est la numéro 〇〇.",
    },
    {
      jp: "チェックアウトは　なんじですか？",
      fr: "A quelle heure dois-je partir ?",
    },
    { jp: "どちらまで", fr: "Où allez-vous ?" },
    { jp: "しょうちしました。", fr: "C'est noté." },
    { jp: "りょうきんは...", fr: "Le prix est..." },
    { jp: "...を　おねがいします。", fr: "... S'il vous plaît." },
    { jp: "...を　ください。", fr: "... S'il vous plaît." },
    { jp: "ありがとうございます。", fr: "Merci beaucoup." },
    { jp: "もういちど", fr: "Encore une fois." },
    {
      jp: "ゆっくり　はなしてください。",
      fr: "Parlez lentement, s'il vous plaît.",
    },
    { jp: "えいごが　はなせますか？", fr: "Pouvez-vous parler anglais ?" },
    { jp: "○○はどこですか?", fr: "Où se trouve ________ ?" },
    {
      jp: "タクシー乗り場（のりば）はどこですか？",
      fr: "Où se trouve la station de taxis ?",
    },
    {
      jp: "○○行（い）きの電車（でんしゃ）/バスはどこですか？",
      fr: "Où se trouve le train/bus pour ____________ ?",
    },
    {
      jp: "でんしゃの切符（きっぷ）はどこで買（か）えますか？",
      fr: "Où puis-je acheter un billet de train ?",
    },
    {
      jp: "最寄り（もより）の駅（えき）はどこですか？",
      fr: "Où se trouve la gare la plus proche ?",
    },
    {
      jp: "〇〇行（い）きはこのホームですか？",
      fr: "C'est bien ce quai pour aller à 〇〇?",
    },
    {
      jp: "チェックインをお願（ねが）いします。",
      fr: "Je voudrais faire le check-in.",
    },
    {
      jp: "チェックアウトは何時（なんじ）ですか？",
      fr: "À quelle heure dois-je partir ?",
    },
    {
      jp: "Wi-Fiのパスワードを教（おし）えてください。",
      fr: "Veuillez m'indiquer le mot de passe pour le Wi-Fi.",
    },
    {
      jp: "朝食（ちょうしょく）は料金（りょうきん）に含（ふく）まれていますか？",
      fr: "Le petit-déjeuner est-il inclus dans le prix ?",
    },
    {
      jp: "朝食（ちょうしょく）はどこですか？",
      fr: "Où est servi le petit-déjeuner ?",
    },
    {
      jp: "朝食（ちょうしょく）は何時（なんじ）から何時（なんじ）ですか？",
      fr: "De quand à quand le petit-déjeuner est-il servi ?",
    },
    {
      jp: "外出（がいしゅつ）するときは鍵（かぎ）を預（あず）けますか？",
      fr: "Dois-je laisser les clefs lorsque je sort ?",
    },
    {
      jp: "荷物（にもつ）を預（あず）かってもらえますか？",
      fr: "Puis-je vous laisser mes bagages ?",
    },
    { jp: "一人（ひとり）です。", fr: "Je suis seul." },
    { jp: "すみません。", fr: "Excusez-moi." },
    { jp: "メニューを見（み）せてください。", fr: "Puis-je voir le menu ?" },
    {
      jp: "お水（みず）をください。",
      fr: "Puis-je avoir de l'eau, s'il vous plaît ?",
    },
    { jp: "〇〇を〇つください。", fr: "Je voudrais 〇 de 〇〇." },
    {
      jp: "クロワッサンを3つください。",
      fr: "Je voudrais trois croissants, s'il vous plaît.",
    },
    {
      jp: "お会計（かいけい）をお願（ねが）いします。",
      fr: "L'addition s'il vous plait.",
    },
    {
      jp: "待（ま）ち時間（じかん）はどれくらいですか？",
      fr: "Combien de temps dois-je attendre ?",
    },
    { jp: "ここで食（た）べます。", fr: "Je les mangerai ici." },
    { jp: "持（も）ち帰（かえ）ります。", fr: "Je les emporterai." },
    { jp: "どれが人気（にんき）ですか？", fr: "Lesquels sont populaires ?" },
    { jp: "オススメはどれですか？", fr: "Lequel recommendez-vous ?" },
    { jp: "◯◯はありますか？", fr: "Vous avez ◯◯ ?" },
    {
      jp: "これを見（み）せてください。",
      fr: "Puis-je voir celui-la s'il vous plait?",
    },
    {
      jp: "他（ほか）の色（いろ）はありますか？",
      fr: "Avez-vous d'autres couleurs ?",
    },
    {
      jp: "もっと大（おお）きい/小（ちい）さいサイズはありますか？",
      fr: "Avez-vous des tailles plus grandes ou plus petites ?",
    },
    { jp: "試着（しちゃく）してもいいですか？", fr: "Puis-je l'essayer ?" },
    { jp: "これはいくらですか？", fr: "Combien coûte celui-ci ?" },
    { jp: "これをください。", fr: "Je le prends s'il vous plait." },
    {
      jp: "クレジットカードは使（つか）えますか？",
      fr: "Puis-je utiliser une carte de crédit ?",
    },
    { jp: "ちょっと考（かんが）えます。", fr: "Je vais réfléchir." },
    { jp: "英語（えいご）が話（はな）せますか？", fr: "Parlez-vous anglais ?" },
    {
      jp: "すみません。わかりません。",
      fr: "Je suis désolé, je ne comprends pas.",
    },
    {
      jp: "もう一度（いちど）お願（ねが）いします。",
      fr: "Encore une fois, s'il vous plaît.",
    },
    {
      jp: "ゆっくり話（はな）してください。",
      fr: "Parlez lentement, s'il vous plaît.",
    },
  ];

  return (
    <StyledMainPaper elevation={3} id="notes">
      <Typography variant="h4" gutterBottom>
        Notes
      </Typography>
      <StyledMainPaper>
        <StyledBox>
          {data.map(({ jp, fr }, index) => (
            <StyledPaper key={`note${index}`} variant="outlined">
              <Typography variant="body1">{jp}</Typography>
              <Typography variant="subtitle2">{fr}</Typography>
            </StyledPaper>
          ))}
        </StyledBox>
      </StyledMainPaper>
    </StyledMainPaper>
  );
};

export default Notes;
