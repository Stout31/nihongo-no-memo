import { useEffect } from "react";

const Furigana = ({ id = "", center, reverse, text }) => {
  const styleNode0 =
    "margin:10px;font-size:x-large;display:flex;flex-wrap:wrap;white-space:nowrap;" +
    `justify-content:${center ? "center" : "flex-start"};` +
    `align-items:${reverse ? "flex-start" : "flex-end"};`;
  const styleNode1 =
    "font-size:x-large;line-height:1;" +
    `align-items:${reverse ? "flex-start" : "flex-end"};`;
  const styleNode2 =
    "font-size:x-large;display:flex;line-height:1;white-space:nowrap;" +
    `flex-direction:${reverse ? "column" : "column-reverse"};`;
  const styleNode3 =
    "font-size:small;margin:0-25px;line-height:1;text-align:center;";

  useEffect(() => {
    let content = text;
    let index1;
    let index2;
    let index3;
    let node0;
    let node1;
    let node2;
    let node3;

    //create parent
    node0 = document.createElement("div");
    node0.style.cssText = styleNode0;

    while (content.length > 0) {
      index1 = content.indexOf("[");
      index2 = content.indexOf("_");
      index3 = content.indexOf("]");

      if (index1 >= 0 && index2 >= 0 && index3 >= 0) {
        // create children
        node1 = document.createElement("div");
        node2 = document.createElement("div");
        node3 = document.createElement("div");

        // fill
        node1.textContent = content.slice(0, index1);
        node2.textContent = content.slice(index1 + 1, index2);
        node3.textContent = content.slice(index2 + 1, index3);

        // style
        node1.style.cssText = styleNode1;
        node2.style.cssText = styleNode2;
        node3.style.cssText = styleNode3;

        // append
        node0.appendChild(node1);
        node0.appendChild(node2);
        node2.appendChild(node3);

        // remove displayed content
        content = content.slice(index3 + 1);
      } else {
        // create child
        node1 = document.createElement("div");

        // fill
        node1.textContent = content;

        // style
        node1.style.cssText = styleNode1;

        // append
        node0.appendChild(node1);

        // remove displayed content
        content = "";
      }
    }

    // replace
    document.getElementById(`furigana${id}`)?.replaceWith(node0);
  }, [
    id,
    center,
    reverse,
    text,
    styleNode0,
    styleNode1,
    styleNode2,
    styleNode3,
  ]);

  return <div id={`furigana${id}`} style={{ display: "none" }}></div>;
};

export default Furigana;
