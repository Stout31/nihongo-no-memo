import { useState } from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";

const Chips = ({ hidden, data, exercise, exercises }) => {
  const [answers, setAnswers] = useState([]);

  return (
    <Box
      sx={{
        height: "350px",
        display: hidden ? "none" : "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <Typography variant="h2" component="div">
        {data.flat().find((d) => d.order === exercise).kana}
      </Typography>
      <Stack direction="row" spacing={1}>
        {answers.map((a) => (
          <Chip
            key={`answers${a}`}
            variant="outlined"
            label={data.flat().find((d) => d.order === a).kana}
            onClick={() => setAnswers(answers.filter((f) => f !== a))}
          />
        ))}
      </Stack>
      <Stack direction="row" spacing={1}>
        {data
          .flat()
          .filter((d) => exercises.find((e) => e.value === d.order))
          .map((d) => (
            <Chip
              key={`exercise${d.order}`}
              variant="outlined"
              label={d.kana}
              onClick={() => setAnswers([...answers, d.order])}
            />
          ))}
      </Stack>
    </Box>
  );
};

export default Chips;
