import useSettings from "../../hooks/use-settings.js";
import styled from "styled-components";
import ToggleButton from "@mui/material/ToggleButton";

const StyledList = styled.div`
  padding: 5px;
  display: flex;
  flex-direction: column;
  gap: 5px;
`;

const Settings = () => {
  const {
    kanjiCol,
    kanaCol,
    romanjiCol,
    frenchCol,
    setKanjiCol,
    setKanaCol,
    setRomanjiCol,
    setFrenchCol,
  } = useSettings();

  return (
    <StyledList>
      WIP
      <ToggleButton
        size="small"
        value="check"
        selected={kanjiCol}
        onChange={() => setKanjiCol(!kanjiCol)}
      >
        Kanji 漢字
      </ToggleButton>
      <ToggleButton
        size="small"
        value="check"
        selected={kanaCol}
        onChange={() => setKanaCol(!kanaCol)}
      >
        Kana かな
      </ToggleButton>
      <ToggleButton
        size="small"
        value="check"
        selected={romanjiCol}
        onChange={() => setRomanjiCol(!romanjiCol)}
      >
        Romanji JP
      </ToggleButton>
      <ToggleButton
        size="small"
        value="check"
        selected={frenchCol}
        onChange={() => setFrenchCol(!frenchCol)}
      >
        Français FR
      </ToggleButton>
    </StyledList>
  );
};

export default Settings;
