import React, { useState, useRef } from "react";
import Traduction from "../exercise/traduction";
import Cards from "../exercise/cards";

const Exercice = ({
  without = [],
  learnList,
  exercice,
  setExercice,
  setDataTarget,
}) => {
  const [exerciceTarget, setExerciceTarget] = useState("traduction");
  const [correct, setCorrect] = useState(true);
  const [progressBar, setProgressBar] = useState(0);
  const [step, setStep] = useState(0);

  const refTraduction = useRef();
  const refCards = useRef();

  return (
    <div
      className="modal fade"
      id="exerciceModal"
      tabIndex="-1"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content bg-dark border border-light">
          <div className="modal-header">
            <h5 className="modal-title" id="modalLabel">
              Exercice avec {learnList.length} mots
            </h5>
            <button
              type="button"
              className="btn text-light fs-4"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => {
                setStep(0);
                setProgressBar(0);
                setCorrect(true);
                setExerciceTarget("traduction");
                setExercice(false);
              }}
            >
              ✖
            </button>
          </div>
          <div className="modal-body">
            <div className="d-flex flex-column align-items-center">
              {!exercice && (
                <ul className="nav nav-tabs ml-auto">
                  <li
                    className={
                      "nav-item" +
                      (!without.find((e) => e === "traduction")
                        ? ""
                        : " d-none")
                    }
                  >
                    <button
                      onClick={() => setExerciceTarget("traduction")}
                      className={`nav-link text-light ${
                        exerciceTarget === "traduction" ? "bg-dark active" : ""
                      }`}
                    >
                      Traduction
                    </button>
                  </li>
                  <li
                    className={
                      "nav-item" +
                      (!without.find((e) => e === "cards") ? "" : " d-none")
                    }
                  >
                    <button
                      onClick={() => setExerciceTarget("cards")}
                      className={`nav-link text-light ${
                        exerciceTarget === "cards" ? "bg-dark active" : ""
                      }`}
                    >
                      Cartes
                    </button>
                  </li>
                </ul>
              )}
              <div className={"w-100 my-4"}>
                <div
                  className={
                    !without.find((e) => e === "traduction") &&
                    exerciceTarget === "traduction"
                      ? ""
                      : " d-none"
                  }
                >
                  <Traduction
                    learnList={learnList}
                    exercice={exercice}
                    setExercice={setExercice}
                    setDataTarget={setDataTarget}
                    correct={correct}
                    setCorrect={setCorrect}
                    setProgressBar={setProgressBar}
                    step={step}
                    setStep={setStep}
                    ref={refTraduction}
                  />
                </div>
                <div
                  className={
                    !without.find((e) => e === "cards") &&
                    exerciceTarget === "cards"
                      ? ""
                      : " d-none"
                  }
                >
                  <Cards
                    learnList={learnList}
                    exercice={exercice}
                    setExercice={setExercice}
                    setDataTarget={setDataTarget}
                    correct={correct}
                    setCorrect={setCorrect}
                    setProgressBar={setProgressBar}
                    step={step}
                    setStep={setStep}
                    ref={refCards}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              onClick={() => {
                if (exerciceTarget === "traduction") {
                  refTraduction.current.handleValidate();
                }
                if (exerciceTarget === "cards") {
                  refCards.current.handleValidate();
                }
              }}
              className={
                "btn btn-lg mb-4 btn-secondary" +
                (exercice && step === learnList.length * 2
                  ? "btn btn-primary"
                  : step % 2
                  ? correct
                    ? " btn-success"
                    : " btn-danger"
                  : "")
              }
              disabled={!learnList.length}
            >
              {!exercice
                ? "Commencer"
                : step === learnList.length * 2
                ? "Terminer"
                : step === learnList.length * 2 - 1
                ? "Résultat"
                : step % 2
                ? "Suivant"
                : "Valider"}
            </button>
            <div className={"progress w-100" + (!exercice ? " d-none" : "")}>
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: progressBar + "%" }}
              ></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Exercice;
