import { useState, useEffect } from "react";

export default function useSettings() {
  const [kanjiCol, setKanjiCol] = useState(
    JSON.parse(localStorage.getItem("kanjiCol"))
  );
  const [kanaCol, setKanaCol] = useState(
    JSON.parse(localStorage.getItem("kanaCol"))
  );
  const [romanjiCol, setRomanjiCol] = useState(
    JSON.parse(localStorage.getItem("romanjiCol"))
  );
  const [frenchCol, setFrenchCol] = useState(
    JSON.parse(localStorage.getItem("frenchCol"))
  );

  useEffect(() => {
    localStorage.setItem("kanjiCol", kanjiCol);
    localStorage.setItem("kanaCol", kanaCol);
    localStorage.setItem("romanjiCol", romanjiCol);
    localStorage.setItem("frenchCol", frenchCol);
  }, [kanjiCol, kanaCol, romanjiCol, frenchCol]);

  return {
    kanjiCol,
    kanaCol,
    romanjiCol,
    frenchCol,
    setKanjiCol,
    setKanaCol,
    setRomanjiCol,
    setFrenchCol,
  };
}
